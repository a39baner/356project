import mysql.connector as mysql
from datetime import datetime



# Replace with own host and username/password
db = mysql.connect(
    host = "marmoset03.shoshin.uwaterloo.ca",
    user = "",
    passwd = ""
)


cursor = db.cursor()

# Replace with own local database where relevant tables exist
cursor.execute("USE db356_a39baner;")




# Splits user input into appropriate format for ingredients and tags
def splitter(input_string, tag):
    temp_list = input_string.split(",")

    output_list = [item.strip(" ") for item in temp_list]

    if(tag):
        output_list = [sub.replace(' ', '-') for sub in output_list]
        


    return output_list

# Creates recipe based on user input (only available to members)
def create_recipe(user_id):
    global max_recipe_id
    global max_ingredient_id
    global max_tag_id

    recipe_name = input("Enter recipe name: ")
    minutes_taken = input("Enter minutes taken (as an integer): ")
    contributor = user_id # Member role
    date_submitted = datetime.today().strftime('%Y-%m-%d')
    tag_input = input("Enter tags seperated by commas: ")
    tag_list = splitter(tag_input, True)
    calories = input("Enter calorie count: ")
    print("\nFor the further nutritional information inputs below, please enter the Percent Daily Value of the nutrient.")
    print("Ex. sugar: 20\n")
    fat = input("Enter total fat: ")
    sugar = input("Enter sugar: ")
    sodium = input("Enter sodium: ")
    protein = input("Enter protein: ")
    saturated_fat = input("Enter saturated fat: ")
    carbs = input("Enter carbohydrates: ")
    steps_count = input("Enter number of steps in recipe: ")
    steps = input("Enter recipe steps seperated by commas: ")
    description = input("Enter recipe description: ")
    ingredients_count = input("Enter number of ingredients: ")
    ingredients = input("Enter ingredients seperated by commas: ")
    ingredient_list = splitter(ingredients, False)
    
    sql_command_insert = "INSERT INTO recipes ( recipe_id, name, minutes, contributer_id, date_submitted, calories,"\
    "fat, sugar, sodium, protein, sat_fat, carbohydrates, n_steps, steps, description ) VALUES (" + str(max_recipe_id) + ", '"\
    + recipe_name + "', " + minutes_taken + ", " + user_id + ", '" + date_submitted + "', " + calories + ", "\
        + fat + ", " + sugar + ", " + sodium + ", " + protein + ", " + saturated_fat + ", " + carbs + ", "\
            + steps_count + ", '" + steps + "', '" + description + "');\n"

    cursor.execute(sql_command_insert)
    
    for i in ingredient_list:
        sql_command_ingredient = "SELECT ingredient_id FROM ingredients WHERE name = '" + i + "' LIMIT 1;"
        cursor.execute(sql_command_ingredient)
        ingredients_id = cursor.fetchall()

        if ingredients_id != []:
            sql_command_ingredient = "INSERT IGNORE INTO recipe_ingredients ( recipe_id, ingredient_id ) VALUES (" + str(max_recipe_id) + ", " + str(ingredients_id[0][0]) + ");\n"
            cursor.execute(sql_command_ingredient)

        else:
            global max_ingredient_id

            sql_command_ingredient = "INSERT INTO ingredients (ingredient_id, name) VALUES (" + str(max_ingredient_id) + ", '" +  i + "');\n"
            cursor.execute(sql_command_ingredient)

            sql_command_ingredient = "INSERT IGNORE INTO recipe_ingredients (recipe_id, ingredient_id) VALUES ("  + str(max_recipe_id) + ", " + str(max_ingredient_id) + ");"
            cursor.execute(sql_command_ingredient)

            max_ingredient_id += 1
    
    for t in tag_list:
        sql_command_tag = "SELECT tag_id FROM tag WHERE name = '" + t + "' LIMIT 1;"
        cursor.execute(sql_command_tag)
        tags_id = cursor.fetchall()

        if tags_id != []:
            sql_command_tag = "INSERT IGNORE INTO recipe_tag ( recipe_id, tag_id ) VALUES (" + str(max_recipe_id) + ", " + str(tags_id[0][0]) + ");\n"
            cursor.execute(sql_command_tag)
        else:
            global max_tag_id

            sql_command_tag = "INSERT INTO tag (tag_id, name) VALUES (" + str(max_tag_id) + ", '" +  t + "');\n"
            cursor.execute(sql_command_tag)

            sql_command_tag = "INSERT IGNORE INTO recipe_tag (recipe_id, tag_id) VALUES ("  + str(max_recipe_id) + ", " + str(max_tag_id) + ");"
            cursor.execute(sql_command_tag)

            max_tag_id += 1
    
    max_recipe_id += 1
    db.commit()
    print("\nRecipe created\n")

# Creates review based on user input (only available to members)
def create_review(user_id):

    # user can only leave one review per recipe?

    contributor = user_id
    recipe_id = input("Enter the Recipe ID of the recipe for this review: ")
    date_submitted = datetime.today().strftime('%Y-%m-%d')
    rating = input("Enter a rating for this review (An integer between 0 and 5): ")
    review_text = input("Enter any comments about the recipe: ")

    sql_create_review_command = "insert into interactions (user_id, recipe_id, date, rating, review) values (" + contributor + ", " + recipe_id + ", " + "'" + date_submitted + "'" + ", " + rating + ", " + "'" + review_text +"'" + ");"
    #print(sql_create_review_command)
    cursor.execute(sql_create_review_command)
    db.commit()
    print("\nReview created\n")

# Edits recipe's attributes based on user input (only available to members who initially created the recipe)
def edit_recipe(user_id):

    search_command = ""

    recipe_id = input("Enter the Recipe ID of the recipe to be edited: ")
    sql_command_check = "SELECT 1 FROM recipes WHERE recipe_id = " + recipe_id
    cursor.execute(sql_command_check)
    isExists = cursor.fetchall()

    while isExists == []:
        recipe_id = input("Invalid Recipe ID. Please enter again: ")
        sql_command_check = "SELECT 1 FROM recipes WHERE recipe_id = " + recipe_id
        cursor.execute(sql_command_check)
        isExists = cursor.fetchall()

    temp_sql_command = "select contributer_id from recipes where contributer_id=" + user_id + " and recipe_id=" + recipe_id + ";"

    cursor.execute(temp_sql_command)
    tables = cursor.fetchall()

    if (len(tables) == 0):
        print("Error: user is not allowed to edit this review")
        return

    search_command = ""

    # if recipe_id.user_id == user_id, allow editing, else they are not allowed to edit
    
    while (search_command != "End edit"):
        search_command = input("What attribute would you like to edit? (Press 'help' for list of edit commands or 'End edit' to exit): ")

        if(search_command == "help"):
            print("\nRecipeID\nUserID\nRecipe Name\nMinutes\nTags\nCalories\nFat\nSugar\nSodium\nProtein\nSaturated Fat\nCarbs\nSteps Count\nIngredients\nRating\n")
        elif(search_command == "Recipe Name"):
            search_attribute = input("Enter new recipe name: ")

            search_command = "UPDATE recipes SET name = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Minutes"):
            search_attribute = input("Enter updated number of minutes: ")

            search_command = "UPDATE recipes SET minutes = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Tags"):
            search_attribute = input("Enter the updated tags seperated by commas: ")
            tag_list = splitter(search_attribute, True)
            
            for t in tag_list:
                sql_command_tag = "SELECT tag_id FROM tag WHERE name = '" + t + "' LIMIT 1;"
                cursor.execute(sql_command_tag)
                tags_id = cursor.fetchall()

                if tags_id != []:
                    sql_command_tag = "INSERT IGNORE INTO recipe_tag ( recipe_id, tag_id ) VALUES (" + str(recipe_id) + ", " + str(tags_id[0][0]) + ");\n"
                    cursor.execute(sql_command_tag)
                else:
                    global max_tag_id

                    sql_command_tag = "INSERT INTO tag (tag_id, name) VALUES (" + str(max_tag_id) + ", '" +  t + "');\n"
                    cursor.execute(sql_command_tag)

                    sql_command_tag = "INSERT IGNORE INTO recipe_tag (recipe_id, tag_id) VALUES ("  + str(recipe_id) + ", " + str(max_tag_id) + ");"
                    cursor.execute(sql_command_tag)

                    max_tag_id += 1

        elif(search_command == "Calories"):
            search_attribute = input("Enter updated calorie count: ")
            
            search_command = "UPDATE recipes SET calories = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Fat"):
            search_attribute = input("Enter updated fat count: ")

            search_command = "UPDATE recipes SET fat = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Sugar"):
            search_attribute = input("Enter updated sugar count: ")

            search_command = "UPDATE recipes SET sugar = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Sodium"):
            search_attribute = input("Enter updated sodium count: ")

            search_command = "UPDATE recipes SET sodium = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Protein"):
            search_attribute = input("Enter updated protein count: ")
            
            search_command = "UPDATE recipes SET protein = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Saturated Fat"):
            search_attribute = input("Enter updated saturated fat count: ")
            
            search_command = "UPDATE recipes SET sat_fat = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Carbs"):
            search_attribute = input("Enter updated carbs count: ")

            search_command = "UPDATE recipes SET carbohydrates = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Steps Count"):
            search_attribute = input("Enter updated steps count: ")
            

            search_command = "UPDATE recipes SET n_steps = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)

        elif(search_command == "Ingredients"):
            search_attribute = input("Enter updated ingredients seperated by commas: ")
            ingredient_list = splitter(search_attribute, False)
            
            for i in ingredient_list:
                sql_command_ingredient = "SELECT ingredient_id FROM ingredients WHERE name = '" + i + "' LIMIT 1;"
                cursor.execute(sql_command_ingredient)
                ingredients_id = cursor.fetchall()

                if ingredients_id != []:
                    sql_command_ingredient = "INSERT IGNORE INTO recipe_ingredients ( recipe_id, ingredient_id ) VALUES (" + str(recipe_id) + ", " + str(ingredients_id[0][0]) + ");\n"                    
                    cursor.execute(sql_command_ingredient)

                else:
                    global max_ingredient_id

                    sql_command_ingredient = "INSERT INTO ingredients (ingredient_id, name) VALUES (" + str(max_ingredient_id) + ", '" +  i + "');\n"
                    cursor.execute(sql_command_ingredient)

                    sql_command_ingredient = "INSERT IGNORE INTO recipe_ingredients (recipe_id, ingredient_id) VALUES ("  + str(recipe_id) + ", " + str(max_ingredient_id) + ");"
                    cursor.execute(sql_command_ingredient)

                    max_ingredient_id += 1

        elif(search_command == "Steps"):
            search_attribute = input("Enter new steps: ")

            search_command = "UPDATE recipes SET steps = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)
        elif(search_command == "Description"):
            search_attribute = input("Enter new description: ")

            search_command = "UPDATE recipes SET description = '" + search_attribute + "' WHERE recipe_id = " + recipe_id
            cursor.execute(search_command)
        elif(search_command == "End edit"):
            continue
        else:
            print("\nError: attribute not found\n")
        
    db.commit()

# Edits review's attributes based on user input (only available to members who initially created the review)
def edit_review(user_id):


    search_command_og = input("Enter the Recipe ID of the recipe from which the review is to be edited: ")

    # if review_id.user_id == user_id, allow editing, else they are not allowed to edit
    sql_command = "select user_id from interactions where user_id=" + user_id + " and recipe_id=" + search_command_og + ";"

    cursor.execute(sql_command)
    tables = cursor.fetchall()

    if (len(tables) == 0):
        print("Error: user is not allowed to edit this review")
        return

    search_command = ""
    while (search_command != "End edit"):
        search_command = input("What attribute would you like to edit? (Press 'help' for list of edit commands or 'End edit' to exit): ")

        if(search_command == "help"):
            print("\nRating\nReview\n")
        elif(search_command == "Rating"):
            rating = input("Enter an updated rating for this review (An integer between 0 and 5): ")
            sql_command = "update interactions set rating=" + "'" + rating + "'" + " where recipe_id=" + search_command_og + ";"
            cursor.execute(sql_command)
            db.commit()
            print("\nUpdated Rating\n")
        elif(search_command == "Review"):
            review_text = input("Enter updated comments about the recipe: ")
            sql_command = "update interactions set review=" + "'" + review_text + "'" + " where recipe_id=" + search_command_og + ";"
            cursor.execute(sql_command)
            db.commit()
            print("\nUpdated Review\n")
        elif(search_command == "End edit"):
            continue
        else:
            print("\nError: attribute not found\n")

# Deletes recipe based on user input (only available to members who initially created the recipe)
def delete_recipe(user_id):

    recipe_id = input("Enter the Recipe ID of the recipe to be deleted: ")

    # if recipe_id.user_id == user_id, allow deleting, else they are not allowed to delete
    sql_command = "select contributer_id from recipes where contributer_id=" + user_id + " and recipe_id=" + recipe_id + ";"

    cursor.execute(sql_command)
    tables = cursor.fetchall()

    if (len(tables) == 0):
        print("Error: user is not allowed to delete this recipe")
        return

    check = input("Are you sure you want to delete this recipe? (y/n): ")
    if(check == "y"):
        # SQL code
        print("Deleting")
        delete = "delete from recipe_ingredients where recipe_id=" + recipe_id + ";"
        cursor.execute(delete)
        delete = "delete from recipe_tag where recipe_id=" + recipe_id + ";"
        cursor.execute(delete)
        delete = "delete from interactions where recipe_id=" + recipe_id + ";"
        cursor.execute(delete)
        delete = "delete from recipes where recipe_id=" + recipe_id + ";"
        cursor.execute(delete)
        db.commit()

# Deletes review based on user input (only available to members who initially created the review)
def delete_review(user_id):
    
    recipe_id = input("Enter the Recipe ID of the recipe from which the review is to be deleted: ")
    
    # if review_id.user_id == user_id, allow deleting, else they are not allowed to delete
    sql_command = "select user_id from interactions where user_id=" + user_id + " and recipe_id=" + recipe_id + ";"

    cursor.execute(sql_command)
    tables = cursor.fetchall()

    if (len(tables) == 0):
        print("Error: user is not allowed to edit this review")
        return

    check = input("Are you sure you want to delete this review? (y/n): ")
    if(check == "y"):
        # SQL code
        delete = "delete from interactions where recipe_id=" + recipe_id + " and user_id=" + user_id + ";"
        cursor.execute(delete)
        print("Deleting")
        db.commit()

# Search by attributes in database
def search():
    search_command = ""

    while(search_command != "End search"):
        search_command = input("What attribute would you like to search by? (Press 'help' for list of search commands or 'End search' to exit): ")

        if(search_command == "help"):
            print("\nRecipeID\nUserID\nRecipe Name\nMinutes\nTags\nCalories\nFat\nSugar\nSodium\nProtein\nSaturated Fat\nCarbs\nSteps Count\nIngredients\nMinimum Rating\nUser Interactions\n")
        elif(search_command == "Recipe Name"):
            search_attribute = input("Enter part or full recipe name to search by: ")
            sql_command = "SELECT * FROM recipes WHERE Name LIKE '%" + search_attribute + "%';"

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE recipes.name LIKE '%" + search_attribute + "%'),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE recipes.name LIKE '%" + search_attribute + "%')"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"
            #print(sql_command)

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")

            # SQL code
        elif(search_command == "Minutes"):
            search_attribute = input("Enter max number of minutes taken for a recipe: ")
            sql_command = "SELECT * FROM recipes WHERE Minutes <= " + search_attribute + ";"
            #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE Minutes <= " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE Minutes <= " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "Tags"):
            search_attribute = input("Enter tags seperated by commas to search by: ")
            tag_list = splitter(search_attribute, True)
            #print(tag_list)
            tag_list_string  = "('" + "', '".join(tag_list) + "')"
            #print(tag_list_string)

            

            sql_command = "WITH recipeIds AS (select distinct recipe_id from recipe_tag inner join tag using (tag_id) WHERE name IN " + tag_list_string + "),"\
                "tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) inner join recipeIds using (recipe_id)), "\
                    "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) inner join recipeIds using (recipe_id))"\
                        "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"


            cursor.execute(sql_command)
            tables = cursor.fetchall()
            print(len(tables))
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "Calories"):
            search_attribute = input("Enter max calorie count to search by: ")
            sql_command = "SELECT * FROM recipes WHERE calories <= " + search_attribute + ";"
            #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE Calories <= " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE Calories <= " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")

            # SQL code
        elif(search_command == "Fat"):
            search_attribute = input("Enter max fat count to search by: ")
            sql_command = "SELECT * FROM recipes WHERE fat <= " + search_attribute + ";"
                        #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE fat <= " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE fat <= " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "Sugar"):
            search_attribute = input("Enter max sugar count to search by: ")
            sql_command = "SELECT * FROM recipes WHERE sugar <= " + search_attribute + ";"
            #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE sugar <= " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE sugar <= " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "Sodium"):
            search_attribute = input("Enter max sodium count to search by: ")
            sql_command = "SELECT * FROM recipes WHERE sodium <= " + search_attribute + ";"
            #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE sodium <= " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE sodium <= " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "Protein"):
            search_attribute = input("Enter max protein count to search by: ")
            sql_command = "SELECT * FROM recipes WHERE protein <= " + search_attribute + ";"
                        #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE protein <= " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE protein <= " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "Saturated Fat"):
            search_attribute = input("Enter max saturated fat to search by: ")
            sql_command = "SELECT * FROM recipes WHERE sat_fat <= " + search_attribute + ";"
            #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE sat_fat <= " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE sat_fat <= " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "Carbs"):
            search_attribute = input("Enter max carbohydrates count to search by: ")
            sql_command = "SELECT * FROM recipes WHERE carbohydrates <= " + search_attribute + ";"
            #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE carbohydrates <= " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE carbohydrates <= " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "Steps Count"):
            search_attribute = input("Enter max number of steps to search by: ")
            sql_command = "SELECT * FROM recipes WHERE n_steps <= " + search_attribute + ";"
            #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE n_steps <= " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE n_steps <= " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "Ingredients"):
            search_attribute = input("Enter ingredients seperated by commas to search by: ")
            ingredient_list = splitter(search_attribute, False)
            #print(ingredient_list)
            ingredients_list_string  = "('" + "', '".join(ingredient_list) + "')"



            sql_command = "WITH recipeIds AS (select distinct recipe_id from recipe_ingredients inner join ingredients using (ingredient_id) WHERE name IN " + ingredients_list_string + "),"\
                "tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) inner join recipeIds using (recipe_id)), "\
                    "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) inner join recipeIds using (recipe_id))"\
                        "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")

            # SQL code
        elif(search_command == "Minimum Rating"):
            search_attribute = input("Enter minimum rating to search by: ")

            #sql_command = "SELECT DISTINCT recipes.* FROM recipes INNER JOIN interactions USING(recipe_id) WHERE rating >= " + search_attribute + ";"

            sql_command = "WITH recipeIds AS (select distinct recipe_id from interactions where rating >= " + search_attribute +   "),"\
                "tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) inner join recipeIds using (recipe_id)), "\
                    "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) inner join recipeIds using (recipe_id))"\
                        "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            #print(sql_command)

            cursor.execute(sql_command)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "RecipeID"):
            search_attribute = input("Enter a RecipeID to search by: ")

            sql_command = "SELECT * FROM recipes WHERE recipe_id = " + search_attribute + ";"
                        #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE recipe_id = " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE recipe_id = " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "UserID"):
            search_attribute = input("Enter a UserID to search by: ")

            sql_command = "SELECT * FROM recipes WHERE contributer_id = " + search_attribute + ";"
                        #print(sql_command)

            sql_command_updated = "with tag_join as (select recipe_id, tag.name as 'tag_name' from recipes inner join recipe_tag using (recipe_id) inner join tag using(tag_id) WHERE contributer_id = " + search_attribute + "),"\
                            "ingredient_join as (select recipe_id, ingredients.name as 'ingredient_name' from recipes inner join recipe_ingredients using (recipe_id) inner join ingredients using(ingredient_id) WHERE contributer_id = " + search_attribute + ")"\
                            "select recipes.*, group_concat(distinct ingredient_name separator ', '), group_concat(distinct tag_name separator ', ') from recipes inner join tag_join using(recipe_id) inner join ingredient_join using (recipe_id) group by recipe_id;"

            cursor.execute(sql_command_updated)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("Recipe Name: " + entry[0])
                    print("Recipe ID: " + str(entry[1]))
                    print("Minutes Taken: " + str(entry[2]))
                    print("Contributor ID: " + str(entry[3]))
                    print("\nSteps: " + entry[13])
                    print("\nDescription: " + entry[14])
                    print("Ingredients: " + entry[15])
                    print("\nTags: " + entry[16])
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")
            # SQL code
        elif(search_command == "User Interactions"):
            search_attribute = input("Enter a RecipeID to search by: ")
            search_attribute1 = input("Enter a UserID to search by: ")

            sql_command = "SELECT * FROM interactions WHERE recipe_id = " + search_attribute + " AND user_id = " + search_attribute1 + ";"
            #print(sql_command)

            cursor.execute(sql_command)
            tables = cursor.fetchall()
            if (len(tables) != 0):
                for entry in tables:
                    print("\n***************\n")
                    print("User ID: " + str(entry[0]))
                    print("Recipe ID: " + str(entry[1]))
                    print("Rating: " + str(entry[3]) + "/5")
                    print("\nReview: " + str(entry[4]))
                   
                    print("\n***************\n")
            else:
                print("\n***************\n")
                print("No recipes found")
                print("\n***************\n")

            #SQL code
        elif(search_command == "End search"):
            continue
        else:
            print("\nError: attribute not found\n")   

def main():
    print('''

    ***************************
               _____          
         /\   / ____|   /\    
        /  \ | |       /  \   
       / /\ \| |      / /\ \  
      / ____ | |____ / ____ \ 
     /_/    \_\_____/_/    \_\
                            

    ***************************
    ''')

    print("Welcome to ACA, the number one stop for all your culinary needs! \nDiscover recipes, reviews, and how-tos from users all around the world right here!")
    command = ""
    user_role = 0

    while(True):
        user_role = int(input("\nHow do you want to access the application? (Press 1 for Guest, 2 for Member): "))

        if(user_role == 1):
            print("\nSigned in as Guest")
            break
        elif (user_role == 2):
            print("\nSigned in as Member")
            user_id = input("Enter an User ID: ")
            break
        else:
            print("\nError logging in. Please select a valid role.")

    print("Type in 'help' to get list of commands and user role descriptions")

    if(user_role == 2):
        sql_command = "WITH recipeID AS (SELECT MAX(recipe_id) AS 'ID' FROM recipes),"\
            "ingredientID AS (SELECT MAX(ingredient_id) AS 'ID' FROM ingredients),"\
            "tagID AS (SELECT MAX(tag_id) AS 'ID' FROM tag)"\
            "SELECT ID FROM recipeID UNION SELECT ID FROM ingredientID UNION SELECT ID FROM tagID"

        cursor.execute(sql_command)
        tables = cursor.fetchall()

        global max_recipe_id
        global max_ingredient_id
        global max_tag_id

        max_recipe_id = tables[0][0] + 1
        max_ingredient_id = tables[1][0] + 1
        max_tag_id = tables[2][0] + 1

    while (command != "quit"):
        
        command = input("\nEnter a command: ")

        if (command == "help"):
            print("\nUser Roles: \n\nGuest: Only able to view/select items from database.")
            print("Member: Able to view, create, delete, and edit items from database\n")

            print("Commands: \n")
            print("quit: Terminates the application")
            print("edit: Members can edit entries from database")
            print("create: Members can create entries to input into database")
            print("delete: Members can delete entries from database")
            print("search: Guests and Members can search the database via specific attributes")
        elif (command == "edit"):
            if(user_role == 1):
                print("\nError: Guest users do not have the permission to edit. Please quit and sign in as a Member to edit.")
            elif(user_role == 2):
                edit_command = int(input("\nWhat would you like to edit? (Press 1 to edit a recipe, 2 to edit a review): "))
                if(edit_command == 1):
                    edit_recipe(user_id)
                elif(edit_command == 2):
                    edit_review(user_id)
        elif (command == "create"):
            if(user_role == 1):
                print("\nError: Guest users do not have the permission to create. Please quit and sign in as a Member to create.")
            elif(user_role == 2):
                create_command = int(input("\nWhat would you like to create? (Press 1 to create a recipe, 2 to create a review): "))
                if(create_command == 1):
                    create_recipe(user_id)
                elif(create_command == 2):
                    create_review(user_id)
        elif (command == "delete"):
            if(user_role == 1):
                print("\nError: Guest users do not have the permission to delete. Please quit and sign in as a Member to delete.")
            elif(user_role == 2):
                delete_command = int(input("\nWhat would you like to delete? (Press 1 to delete a recipe, 2 to delete a review): "))
                if(delete_command == 1):
                    delete_recipe(user_id)
                elif(delete_command == 2):
                    delete_review(user_id)
        elif (command == "search"):
            search()
        elif (command == "quit"):
            continue
        else:
            print("Error: Not a valid command")

main()
